#include "lwip/opt.h"
#include "lwip/arch.h"
#include "lwip/api.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "tcp_echoserver_socket.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define TCP_ECHO_SERVER_THREAD_PRIO    ( tskIDLE_PRIORITY + 4 )

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/**
  * @brief  tcp echo server thread 
  * @param arg: pointer on argument(not used here) 
  * @retval None
  */
static void tcp_echo_server_socket_thread(void *arg)
{
  int sock;
  int conn;
  int size;
  int ret;
  struct sockaddr_in address;
  struct sockaddr_in remotehost;
  unsigned char recv_buffer[1500];
  
 /* create a TCP socket */
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
  {
    return;
  }
  
  /* bind to port 7 (ECHO protocol) at any interface */
  address.sin_family = AF_INET;
  address.sin_port = htons(7);
  address.sin_addr.s_addr = INADDR_ANY;

  if (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
  {
    return;
  }
  
  /* listen for incoming connections (TCP listen backlog = 5) */
  listen(sock, 5);
  
  size = sizeof(remotehost);
  
  while (1) 
  {
    conn = accept(sock, (struct sockaddr *)&remotehost, (socklen_t *)&size);
    
    do
    {
      /* Read in the request */
      ret = read(conn, recv_buffer, sizeof(recv_buffer));
      
      if(ret <= 0)
      {
        /* remote host closed connection */
        break;
      }
      
      /* send back the received data (echo) */
      write(conn, (const unsigned char*)recv_buffer, ret);
      
    }while(ret > 0);
    
    /* Close connection socket */
    close(conn);
  }
}

/**
  * @brief  Initialize the TCP Echo server (start its thread) 
  * @param  none
  * @retval None
  */
void tcp_echo_server_socket_init()
{
  sys_thread_new("TCP_ECHO", tcp_echo_server_socket_thread, NULL, DEFAULT_THREAD_STACKSIZE * 2, TCP_ECHO_SERVER_THREAD_PRIO);
}
